#pragma once
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <algorithm>
#include <vector>

using namespace std;

class Skorost //пространство скоростей
{
public:
	int N_ksi;
	double ksicut;
	vector <double> ksi_x;
	vector <double> ksi_y;
	vector <double> ksi_z;

	vector <double> v_x;
	vector <double> v_y;
	vector <double> v_z;

	vector <int> it_v;
	
	Skorost(int nksi, double kc){
	N_ksi=nksi;
	ksicut=kc;
	}
	
	void make_skorost();
private:

	int i,j,k;
};
