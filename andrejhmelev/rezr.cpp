#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <algorithm>
#include <vector>

#include "rezr.hpp"
#include "skorost.hpp"


using namespace std;

void Rezr::make_cells_y()
{
	//создаём сетку по у
		hy_min=Ly/Ny;
		a0=0.2;//alfa
		a1=0;
		
		for(p=0;p<=Ny+dNy;p++) {

			hy.push_back(0);
			Y.push_back(0);
			Y05.push_back(0);
			
		}

		for(p=1;p<=Ny;p++) {

			hy[p]=hy_min;
		}

		while((hy_min*(pow(1+a0,dNy)-1)/a0-dLy)>0.0000001)
		{

			a1=a0-(a0*dLy-hy_min*(pow(1+a0,dNy)-1))/(dLy-hy_min*dNy*pow(1+a0,dNy-1));
			a0=a1;

		}

		for(p=1;p<=dNy;p++) {

			hy[Ny+p]=hy_min*pow(1+a0,p-1);

		}
		
		Y05[0]=0;
		Y[0]=Y05[0]-hy[1]/2;

		for(p=1;p<=Ny+dNy;p++){

			Y05[p]=Y05[p-1]+hy[p];
			Y[p]=Y05[p-1]+hy[p]/2;

		}
		
		Y.push_back(Y05[Ny+dNy]+hy_min*pow(1+a0,dNy)/2);
}
	
void Rezr::make_cells_x()
{
	//создаём сетку по х
		hx_min=Ly/Ny;
		a0=0.2;//alfa
		a1=0;

		while((hx_min*(pow(1+a0,Nx)-1)/a0-Lx)>0.0000001)
		{
			a1=a0-(a0*Lx-hx_min*(pow(1+a0,Nx)-1))/(Lx-hx_min*Nx*pow(1+a0,Nx-1));
			a0=a1;
		}

		for(p=0;p<=Nx+2;p++) {

			hx.push_back(0);
			X.push_back(0);
			X05.push_back(0);
			
		}		

		for(p=2;p<=Nx+2;p++) {

			hx[p]=hx_min*pow(1+a0,p-2);

		}

		for(p=2;p<=Nx+2;p++){

			X05[p]=X05[p-1]+hx[p];
			X[p]=X05[p-1]+hx[p]/2;
		}
	

}

void Rezr::make_kurant()
{
		for(p=0;p<v_x.size()*hx.size();p++){
			kur_x.push_back(0);
		}

		for(p=0;p<v_x.size();p++){
			for(j=0;j<hx.size();j++){

			kur_x[p+v_y.size()*j]=fabs(v_x[p])/ksicut*hx_min/hx[j];

			}
		}

		for(p=0;p<v_y.size()*hy.size();p++){
			kur_y.push_back(0);
		}

		for(p=0;p<v_y.size();p++){
			for(j=1;j<hy.size();j++){
			
			kur_y[p+v_y.size()*j]=fabs(v_y[p])/ksicut*hy_min/hy[j];

			}
		}
	
}


void Rezr::make_const()
{
	
	for(p=0;p<v_y.size();p++){

	Eotr_x.push_back(0);
	Epol_x.push_back(0);

	Eotr_y.push_back(0);
	Epol_y.push_back(0);

	}

	Sum_Eotr_x=0;
	Sum_Epol_x=0;

	Sum_Eotr_y=0;
	Sum_Epol_y=0;

	for(p=0;p<v_y.size();p++){
		if(v_x[p]<0){
		Eotr_x[p]=exp(((double) -1/2)*T_Rezl/T_Rezr*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
		Sum_Eotr_x=Sum_Eotr_x+fabs(v_x[p])*Eotr_x[p];
		}
		if(v_x[p]>0){
		Epol_x[p]=exp(((double) -1/2)*T_Rezl/T_Rezr*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
		Sum_Epol_x=Sum_Epol_x+fabs(v_x[p])*Epol_x[p];
		}
		if(v_y[p]<0){
		Eotr_y[p]=exp(((double) -1/2)*T_Rezl/T_Rezr*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
		Sum_Eotr_y=Sum_Eotr_y+fabs(v_y[p])*Eotr_y[p];
		}
		if(v_y[p]>0){
		Epol_y[p]=exp(((double) -1/2)*T_Rezl/T_Rezr*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
		Sum_Epol_y=Sum_Epol_y+fabs(v_y[p])*Epol_y[p];
		}
	}
	
}


void Rezr::make_F_begin()
{
	C=0;

	for(p=0;p<v_y.size()*hx.size()*(hy.size()+1);p++){

		F0.push_back(0);
		F05.push_back(0);
		th05.push_back(0);
		fi05.push_back(0);

	}

	//константа распределения
	for(p=0;p<v_y.size();p++){

		C=C+exp(((double) -1/2)*T_Rezl/T_Rezl*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
	}
	
	C=1/C*(N_ksi*N_ksi)/(ksicut*ksicut);

	//начальное условие
	for(i=2;i<hx.size()-1;i++){//по х
		for(p=0;p<v_y.size();p++){//по скоростям
			for(j=1;j<hy.size();j++){//по у

				//F0[i+hx.size()*p+v_y.size()*hx.size()*j]=C*exp(((double) -1/2)*T_Rezl/T_Rezl*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
				F0[i+hx.size()*p+v_y.size()*hx.size()*j]=0;

			}
		}
	}
}


void Rezr::get_M()
{
	M=0;
	M_pol=0;
	M_otr=0;
	for(i=2;i<hx.size()-1;i++){//по х
		for(p=0;p<v_y.size();p++){//по скоростям
			for(j=1;j<hy.size();j++){//по у
				M=M+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];
				if(v_x[p]>0){
				M_pol=M_pol+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];
				} else {
				M_otr=M_otr+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];	
				}
			}
		}
	}

}


int Rezr::make_Fyt(double l)
{
	vx=v_y.size()*hx.size();

	for(i=2;i<hx.size()-1;i++){//по всем столбцам по х
		for(p=0;p<v_y.size();p++){//по всем скоростям

			it_vx=i+hx.size()*p;
		
			if(v_y[p]>0) {

				F0[it_vx+vx*hy.size()]=std::max((double) 0,(double) 2*F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);

				for(j=2;j<=hy.size()-1;j++){
					if(((F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j])*(F0[it_vx+vx*j]-F0[it_vx+vx*(j-1)]))<=0){

						th05[it_vx+vx*j]=0;
						fi05[it_vx+vx*j]=0;

					} else {	
				
						th05[it_vx+vx*j]=(F0[it_vx+vx*j]-F0[it_vx+vx*(j-1)])/(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

						fi05[it_vx+vx*j]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*j])/kur_y[p+v_y.size()*j]*th05[it_vx+vx*j],
							(double) hy[j]/(((double) 2)*(Y[j+1]-Y[j-1]))*(1-kur_y[p+v_y.size()*j])*(1+th05[it_vx+vx*j]))));

					}

				F05[it_vx+vx*j]=F0[it_vx+vx*j]+fi05[it_vx+vx*j]*(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);	

				}

			} else {

				F0[it_vx+vx*0]=std::max((double) 0,(double) 2*F0[it_vx+vx*1]-F0[it_vx+vx*2]);

				for(j=0;j<=hy.size()-3;j++){
					if(((F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j])*(F0[it_vx+vx*(j+2)]-F0[it_vx+vx*(j+1)]))<=0){

						th05[it_vx+vx*j]=0;
						fi05[it_vx+vx*j]=0;

					} else {
	
						th05[it_vx+vx*j]=(F0[it_vx+vx*(j+2)]-F0[it_vx+vx*(j+1)])/(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

						fi05[it_vx+vx*j]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*(j+1)])/kur_y[p+v_y.size()*(j+1)]*th05[it_vx+vx*j],
						(double) hy[j+1]/(((double) 2)*(Y[j+2]-Y[j])))*(1-kur_y[p+v_y.size()*(j+1)]*(1+th05[it_vx+vx*j]))));
					}

				F05[it_vx+vx*j]=F0[it_vx+vx*(j+1)]-fi05[it_vx+vx*j]*(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

				}
			}
	
		}

//дополнительные точки

		//F05[0] and F0[0] для положительных скоростей
				
		for (p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){
				for(k=0;k<v_y.size();k++){

					if((fabs(v_y[k]-(-1)*v_y[p])<0.000001) && (fabs(v_y[k]-(-1)*v_y[p])>=0) && (fabs(v_x[k]-v_x[p])<0.000001) && (fabs(v_x[k]-v_x[p])>=0) && (fabs(v_z[k]-v_z[p])<0.000001) && (fabs(v_z[k]-v_z[p])>=0))
					{

					F05[it_vx+vx*0]=F05[i+hx.size()*k+vx*0];
					F0[it_vx+vx*0]=F0[i+hx.size()*k+vx*1];

					}
				}
			}
		}



		//F05[N] otr

		Sum_Vpol_y=0;
	
		for (p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){

				Sum_Vpol_y=Sum_Vpol_y+fabs(v_y[p])*F05[it_vx+vx*(hy.size()-1)];

			}
		}
			
		for (p=0; p<it_v.size();p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]<0){

				F05[it_vx+vx*(hy.size()-1)]=Sum_Vpol_y/Sum_Epol_y*Eotr_y[p];

			}
		}



		//F05[1] pol
	
		for(p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){
				if(((F0[it_vx+vx*2]-F0[it_vx+vx*1])*(F0[it_vx+vx*1]-F0[it_vx+vx*0]))<=0){

					th05[it_vx+vx*1]=0;
					fi05[it_vx+vx*1]=0;

				} else {
					
					th05[it_vx+vx*1]=(F0[it_vx+vx*1]-F0[it_vx+vx*0])/(F0[it_vx+vx*2]-F0[it_vx+vx*1]);

					fi05[it_vx+vx*1]=std::max((double) 0,
							std::min((double) 1,
							std::min((double) (1-kur_y[p+v_y.size()*1])/kur_y[p+v_y.size()*1]*th05[it_vx+vx*1],
							(double) hy[1]/(((double) 2)*(Y[2]-Y[0]))*(1-kur_y[p+v_y.size()*1])*(1+th05[it_vx+vx*1]))));
				}

			F05[it_vx+vx*1]=F0[it_vx+vx*1]+fi05[it_vx+vx*1]*(F0[it_vx+vx*2]-F0[it_vx+vx*1]);

			}
		}	
	



		//F05[N-1] otr

		Sum_Vpol_y=0;

		for (p=0; p<v_y.size(); p++) {

			it_vx=i+hx.size()*p;

			if(v_y[p]>0){
			
			Sum_Vpol_y=Sum_Vpol_y+fabs(v_y[p])*(F0[it_vx+vx*(hy.size()-1)]+F0[it_vx+vx*hy.size()])/2;

			}
		}

		g_otr=0;

		for(p=0; p<v_y.size(); p++) {

			it_vx=i+hx.size()*p;

			if(v_y[p]<0){

			g_otr=Sum_Vpol_y/Sum_Epol_y*Eotr_y[p];

			F0[it_vx+vx*hy.size()]=std::max((double) 0,(double) 2*g_otr-F0[it_vx+vx*(hy.size()-1)]);

				if(((F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)])*(F0[it_vx+vx*hy.size()]-F0[it_vx+vx*(hy.size()-1)]))<=0){
					th05[it_vx+vx*(hy.size()-2)]=0;
					fi05[it_vx+vx*(hy.size()-2)]=0;

				} else {

					th05[it_vx+vx*(hy.size()-2)]=(F0[it_vx+vx*hy.size()]-F0[it_vx+vx*(hy.size()-1)])/
									(F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);


					fi05[it_vx+vx*(hy.size()-2)]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*(hy.size()-1)])/kur_y[p+v_y.size()*(hy.size()-1)]*th05[it_vx+vx*(hy.size()-2)],
								(double) hy[hy.size()-1]/(((double) 2)*(Y[hy.size()]-Y[hy.size()-2]))*(1-kur_y[p+v_y.size()*(hy.size()-1)])*(1+th05[it_vx+vx*(hy.size()-2)]))));

				}

			F05[it_vx+vx*(hy.size()-2)]=F0[it_vx+vx*(hy.size()-1)]-fi05[it_vx+vx*(hy.size()-2)]*(F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);

			g_otr=0;

			}
		}




		//новый слой

		for(p=0;p<v_y.size();p++){

		it_vx=i+hx.size()*p;

			for(j=1;j<hy.size();j++){

				if(v_y[p]<0){

				F0[it_vx+vx*j]=F0[it_vx+vx*j]+l*kur_y[p+v_y.size()*j]*(F05[it_vx+vx*j]-F05[it_vx+vx*(j-1)]);

				} else {

				F0[it_vx+vx*j]=F0[it_vx+vx*j]-l*kur_y[p+v_y.size()*j]*(F05[it_vx+vx*j]-F05[it_vx+vx*(j-1)]);

				}
			}
		}	

	}

return 0;
}

void Rezr::make_Fxt() 
{
	
	for(j=1;j<hy.size();j++){//по всем столбцам по y
	vy=v_y.size()*hx.size()*j;
	if(j>Ny) {

		for(p=0;p<v_x.size();p++){//по всем скоростям

			it_vy=hx.size()*p+vy;
		
			if(v_x[p]>0) {

				F0[(hx.size()-1)+it_vy]=std::max((double) 0,(double) 2*F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);

				for(i=3;i<=hx.size()-2;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[i+it_vy]-F0[(i-1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {	
				
						th05[i+it_vy]=(F0[i+it_vy]-F0[(i-1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*i])/kur_x[p+v_x.size()*i]*th05[i+it_vy],
							(double) hx[i]/(((double) 2)*(X[i+1]-X[i-1]))*(1-kur_x[p+v_x.size()*i])*(1+th05[i+it_vy]))));

					}

				F05[i+it_vy]=F0[i+it_vy]+fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);	

				}

			} else {

				F0[1+it_vy]=std::max((double) 0,(double) 2*F0[2+it_vy]-F0[3+it_vy]);

				for(i=1;i<=hx.size()-4;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[(i+2)+it_vy]-F0[(i+1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {
	
						th05[i+it_vy]=(F0[(i+2)+it_vy]-F0[(i+1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*(i+1)])/kur_x[p+v_x.size()*(i+1)]*th05[i+it_vy],
						(double) hx[i+1]/(((double) 2)*(X[i+2]-X[i])))*(1-kur_x[p+v_x.size()*(i+1)]*(1+th05[i+it_vy]))));
					}

				F05[i+it_vy]=F0[(i+1)+it_vy]-fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);

				}
			}
	
		}

//дополнительные точки


		//F05[0] для положительных скоростей
				
		Sum_Votr_x=0;
	
		for (p=0; p<v_x.size(); p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]<0){
				Sum_Votr_x=Sum_Votr_x+fabs(v_x[p])*F05[1+it_vy];

			}
		}
			
		for (p=0; p<v_x.size();p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]>0){

				F05[1+it_vy]=Sum_Votr_x/Sum_Eotr_x*Epol_x[p];

			}
		}

		//F05[N] otr

		Sum_Vpol_x=0;
	
		for (p=0; p<v_x.size(); p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]>0){
				Sum_Vpol_x=Sum_Vpol_x+fabs(v_x[p])*F05[(hx.size()-2)+it_vy];

			}
		}
			
		for (p=0; p<v_x.size();p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]<0){

				F05[(hx.size()-2)+it_vy]=Sum_Vpol_x/Sum_Epol_x*Eotr_x[p];

			}

		}



		//F05[1] pol
	
		Sum_Votr_x=0;

		for (p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]<0){
			
			Sum_Votr_x=Sum_Votr_x+fabs(v_x[p])*(F0[2+it_vy]+F0[3+it_vy])/2;

			}
		}

		g_pol_x=0;

		for(p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]>0){

			g_pol_x=Sum_Votr_x/Sum_Eotr_x*Epol_x[p];

			F0[1+it_vy]=std::max((double) 0,(double) 2*g_pol_x-F0[2+it_vy]);

				if(((F0[3+it_vy]-F0[2+it_vy])*(F0[2+it_vy]-F0[1+it_vy]))<=0){

					th05[2+it_vy]=0;
					fi05[2+it_vy]=0;

				} else {
					
					th05[2+it_vy]=(F0[2+it_vy]-F0[1+it_vy])/(F0[3+it_vy]-F0[2+it_vy]);

					fi05[2+it_vy]=std::max((double) 0,
							std::min((double) 1,
							std::min((double) (1-kur_x[p+v_x.size()*2])/kur_x[p+v_x.size()*2]*th05[2+it_vy],
							(double) hx[2]/(((double) 2)*(X[3]-X[1]))*(1-kur_x[p+v_x.size()*2])*(1+th05[2+it_vy]))));
				}

			F05[2+it_vy]=F0[2+it_vy]+fi05[2+it_vy]*(F0[3+it_vy]-F0[2+it_vy]);

			}
			
			g_pol_x=0;

		}



		//F05[N-1] otr

		Sum_Vpol_x=0;

		for (p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]>0){
			
			Sum_Vpol_x=Sum_Vpol_x+fabs(v_x[p])*(F0[(hx.size()-2)+it_vy]+F0[(hy.size()-3)+it_vy])/2;

			}
		}

		g_otr_x=0;

		for(p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]<0){

			g_otr_x=Sum_Vpol_x/Sum_Epol_x*Eotr_x[p];

			F0[(hx.size()-1)+it_vy]=std::max((double) 0,(double) 2*g_otr_x-F0[(hx.size()-2)+it_vy]);

				if(((F0[(hx.size()-1)+it_vy]-F0[(hx.size()-2)+it_vy])*(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]))<=0){
					th05[(hx.size()-3)+it_vy]=0;
					fi05[(hx.size()-3)+it_vy]=0;

				} else {

					th05[(hx.size()-3)+it_vy]=(F0[(hx.size()-1)+it_vy]-F0[(hx.size()-2)+it_vy])/
									(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);


					fi05[(hx.size()-3)+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*(hx.size()-2)])/kur_x[p+v_x.size()*(hx.size()-2)]*th05[(hx.size()-3)+it_vy],
								(double) hx[hx.size()-2]/(((double) 2)*(X[hx.size()-1]-X[hx.size()-3]))*(1-kur_x[p+v_x.size()*(hx.size()-2)])*(1+th05[(hx.size()-3)+it_vy]))));

				}

			F05[(hx.size()-3)+it_vy]=F0[(hx.size()-2)+it_vy]-fi05[(hx.size()-3)+it_vy]*(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);

			g_otr_x=0;

			}
		}


		//новый слой

		for(p=0;p<v_x.size();p++){

		it_vy=hx.size()*p+vy;

			for(i=2;i<hx.size()-1;i++){

				if(v_x[p]<0){

				F0[i+it_vy]=F0[i+it_vy]+kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				} else {

				F0[i+it_vy]=F0[i+it_vy]-kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				}
				
			}
		}





	} else {

		for(p=0;p<v_x.size();p++){//по всем скоростям

			it_vy=hx.size()*p+vy;
		
			if(v_x[p]>0) {

				F0[(hx.size()-1)+it_vy]=std::max((double) 0,(double) 2*F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);

				for(i=1;i<=hx.size()-2;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[i+it_vy]-F0[(i-1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {	
				
						th05[i+it_vy]=(F0[i+it_vy]-F0[(i-1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*i])/kur_x[p+v_x.size()*i]*th05[i+it_vy],
							(double) hx[i]/(((double) 2)*(X[i+1]-X[i-1]))*(1-kur_x[p+v_x.size()*i])*(1+th05[i+it_vy]))));

					}

				F05[i+it_vy]=F0[i+it_vy]+fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);	

				}

			} else {

				for(i=1;i<=hx.size()-4;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[(i+2)+it_vy]-F0[(i+1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {
	
						th05[i+it_vy]=(F0[(i+2)+it_vy]-F0[(i+1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*(i+1)])/kur_x[p+v_x.size()*(i+1)]*th05[i+it_vy],
						(double) hx[i+1]/(((double) 2)*(X[i+2]-X[i])))*(1-kur_x[p+v_x.size()*(i+1)]*(1+th05[i+it_vy]))));
					}

				F05[i+it_vy]=F0[(i+1)+it_vy]-fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);

				}
			}
	
		}

//дополнительные точки


		//F05[N] для отрицвтельных скоростей
				
		Sum_Vpol_x=0;
	
		for (p=0; p<v_x.size(); p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]>0){
				Sum_Vpol_x=Sum_Vpol_x+fabs(v_x[p])*F05[(hx.size()-2)+it_vy];

			}
		}
			
		for (p=0; p<v_x.size();p++) {

		it_vy=hx.size()*p+vy;

			if(v_x[p]<0){

				F05[(hx.size()-2)+it_vy]=Sum_Vpol_x/Sum_Epol_x*Eotr_x[p];

			}
		}

	
		//F05[N-1] otr
	
		Sum_Vpol_x=0;

		for (p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]>0){
			
			Sum_Vpol_x=Sum_Vpol_x+fabs(v_x[p])*(F0[(hx.size()-2)+it_vy]+F0[(hx.size()-3)+it_vy])/2;

			}
		}

		g_otr_x=0;

		for(p=0; p<v_x.size(); p++) {
			it_vy=hx.size()*p+vy;

			if(v_x[p]<0){

			g_otr_x=Sum_Vpol_x/Sum_Epol_x*Eotr_x[p];

			F0[(hx.size()-1)+it_vy]=std::max((double) 0,(double) 2*g_otr_x-F0[(hx.size()-2)+it_vy]);

				if(((F0[(hx.size()-1)+it_vy]-F0[(hx.size()-2)+it_vy])*(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]))<=0){

					th05[(hx.size()-3)+it_vy]=0;
					fi05[(hx.size()-3)+it_vy]=0;

				} else {
					
					th05[(hx.size()-3)+it_vy]=(F0[(hx.size()-1)+it_vy]-F0[(hx.size()-2)+it_vy])/(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);

					fi05[(hx.size()-3)+it_vy]=std::max((double) 0,
							std::min((double) 1,
							std::min((double) (1-kur_x[p+v_x.size()*(hx.size()-2)])/kur_x[p+v_x.size()*(hx.size()-2)]*th05[(hx.size()-3)+it_vy],
							(double) hx[(hx.size()-2)]/(((double) 2)*(X[(hx.size()-1)]-X[(hx.size()-3)]))*(1-kur_x[p+v_x.size()*(hx.size()-2)])*(1+th05[(hx.size()-3)+it_vy]))));
				}

			F05[(hx.size()-3)+it_vy]=F0[(hx.size()-2)+it_vy]-fi05[(hx.size()-3)+it_vy]*(F0[(hx.size()-2)+it_vy]-F0[(hx.size()-3)+it_vy]);

			}
			
			g_otr_x=0;

		}

		//новый слой

		for(p=0;p<v_x.size();p++){

		it_vy=hx.size()*p+vy;

			for(i=2;i<hx.size()-1;i++){

				if(v_x[p]<0){

				F0[i+it_vy]=F0[i+it_vy]+kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				} else {

				F0[i+it_vy]=F0[i+it_vy]-kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				}
				
			}
		}


	}


}
}
