#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <algorithm>
#include <vector>
#include <mpi.h>
#include <string.h>

#include "skorost.hpp"
#include "rezl.hpp"
#include "rezr.hpp"
#include "trubka.hpp"

using namespace std;


int main(int argc, char **argv) 
{

//общие настройки

	int myid, numprocs;
 	double startwtime = 0.0, endwtime;
	int namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];

	MPI_Status cStatus;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Get_processor_name(processor_name,&namelen);
 	
//время работы	
	int t=1;


//обработка левого резервуара
if(myid==0)
{
	startwtime = MPI_Wtime();
	
	int save0;
	int i0;
	int p0;
	int j0;
	int n0;

	double pr;

	double M01_begin=0,M02_begin=0,M00_begin=0;
	double M01_end=0,M02_end=0,M00_end=0;
	double M_begin;
	double M_end;

	int sloev0;

	//создаём левый резервуар
	int nx0,ny0,dny0;
	double lx0,ly0,dly0,TL0,TR0;

	int nksi0;
	double kc0;

	ifstream fin0 ("rezl.txt"); //открытие файла для ввода
	fin0>>lx0>>ly0>>dly0>>nx0>>ny0>>dny0>>TL0>>TR0;
	fin0.close();

	ifstream fin00 ("skorost.txt"); //открытие файла для ввода
	fin00>>nksi0>>kc0;
	fin00.close();

	Rezl rezl(lx0,ly0,dly0,nx0,ny0,dny0,TL0,TR0,nksi0,kc0);

	rezl.make_skorost();

	rezl.make_cells_y();//создаём ячейки и сетку по у
	rezl.make_cells_x();//создаём ячейки и сетку по х

	MPI_Barrier(MPI_COMM_WORLD);

	//недостоющие элементы сетки
	
	MPI_Send(&rezl.hx[rezl.hx.size()-4],1,MPI_DOUBLE,1,1,MPI_COMM_WORLD);
	MPI_Send(&rezl.hx[rezl.hx.size()-3],1,MPI_DOUBLE,1,2,MPI_COMM_WORLD);

	MPI_Recv(&rezl.hx[rezl.hx.size()-2],1,MPI_DOUBLE,1,3,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&rezl.hx[rezl.hx.size()-1],1,MPI_DOUBLE,1,4,MPI_COMM_WORLD,&cStatus);
	
	MPI_Recv(&rezl.X[rezl.X.size()-2],1,MPI_DOUBLE,1,3,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&rezl.X[rezl.X.size()-1],1,MPI_DOUBLE,1,4,MPI_COMM_WORLD,&cStatus);

	rezl.X[rezl.X.size()-2]=rezl.Lx+rezl.X[rezl.X.size()-2];
	rezl.X[rezl.X.size()-1]=rezl.Lx+rezl.X[rezl.X.size()-1];


	rezl.make_kurant();//создаём числа куранта
	rezl.make_const();//создаём константы
	rezl.make_F_begin();//создаём начальное условие	

	//загрузка функции распределения из файла
	cout<<"Загрузить F0?\n";
	cout<<"Если да, нажмите 1\n";
	cin >> save0;

	MPI_Send(&save0,1,MPI_DOUBLE,1,1,MPI_COMM_WORLD);
	MPI_Send(&save0,1,MPI_DOUBLE,2,2,MPI_COMM_WORLD);

	if(save0==1){
		ifstream fin0 ("F0_rezl.txt"); //открытие файла для ввода

		for(p0=0;p0<rezl.it_v.size()*rezl.hx.size()*(rezl.hy.size()+1);p0++){
		
		fin0>>rezl.F0[p0];
	
		}

		fin0.close();
	}

	rezl.get_M();
	M00_begin=rezl.M;
	
	//временные слои
	sloev0=t/rezl.hy_min*rezl.ksicut;

	for(n0=1;n0<=sloev0;n0++){
		if(n0==1){
			rezl.make_Fyt(0.5);
		} else {
			rezl.make_Fyt(1.0);
		}
	
	//передаём данные
	for(j0=1;j0<=rezl.Ny;j0++){
		for(p0=0;p0<rezl.v_y.size();p0++){

	MPI_Recv(&rezl.F0[(rezl.hx.size()-1)+rezl.hx.size()*p0+rezl.v_y.size()*rezl.hx.size()*j0],1,MPI_DOUBLE,1,1,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&rezl.F0[(rezl.hx.size()-2)+rezl.hx.size()*p0+rezl.v_y.size()*rezl.hx.size()*j0],1,MPI_DOUBLE,1,2,MPI_COMM_WORLD,&cStatus);

	MPI_Send(&rezl.F0[(rezl.hx.size()-3)+rezl.hx.size()*p0+rezl.v_y.size()*rezl.hx.size()*j0],1,MPI_DOUBLE,1,3,MPI_COMM_WORLD);
	MPI_Send(&rezl.F0[(rezl.hx.size()-4)+rezl.hx.size()*p0+rezl.v_y.size()*rezl.hx.size()*j0],1,MPI_DOUBLE,1,4,MPI_COMM_WORLD);

		}
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
		rezl.make_Fxt();

		if(n0==sloev0){
			rezl.make_Fyt(0.5);
		} 	
		
	
		pr=(double) n0/sloev0*100;
		cout<<"done "<<pr<<"%"<<"\n";
	}

	rezl.get_M();
	M00_end=rezl.M;
		

	//получем данные от других процессов
	MPI_Recv(&M01_begin,1,MPI_DOUBLE,1,98,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&M02_begin,1,MPI_DOUBLE,2,99,MPI_COMM_WORLD,&cStatus);

	MPI_Recv(&M01_end,1,MPI_DOUBLE,1,96,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&M02_end,1,MPI_DOUBLE,2,97,MPI_COMM_WORLD,&cStatus);

	cout<<"\n \n \n";
	cout << "rezl_M_begin="<< setprecision(15)<<M00_begin<<"\n";
	cout<<"\n"; 
	cout << "rezl_M_end="<< setprecision(15)<< M00_end << "\n";
	cout<<"\n";
	cout << "delta_rezl_M_end="<< setprecision(15)<< M00_end-M00_begin << "\n";
	cout<<"\n";
	cout << "trubka_M_begin="<< setprecision(15)<<M01_begin<<"\n";
	cout<<"\n"; 
	cout << "trubka_M_end="<< setprecision(15)<< M01_end << "\n";
	cout<<"\n";
	cout << "delta_trubka_M_end="<< setprecision(15)<< M01_end-M01_begin << "\n";
	cout<<"\n";
	cout << "rezr_M_begin="<< setprecision(15)<<M02_begin<<"\n";
	cout<<"\n"; 
	cout << "rezr_M_end="<< setprecision(15)<< M02_end << "\n";
	cout<<"\n";
	cout << "delta_rezr_M_end="<< setprecision(15)<< M02_end-M02_begin << "\n";
	cout<<"\n \n \n";

	M_begin=M00_begin+M01_begin+M02_begin;
	M_end=M00_end+M01_end+M02_end;

	MPI_Barrier(MPI_COMM_WORLD);

	cout<<"sloev="<<sloev0<<"\n \n";
	 
	cout << "M_begin="<< setprecision(15)<<M_begin<<"\n";
	cout<<"\n"; 
	cout << "M_end="<< setprecision(15)<< M_end << "\n";
	cout<<"\n";
	cout << "delta M="<< setprecision(15)<<M_end-M_begin<<"\n";
	cout<<"\n";
	cout<<"\n \n \n";	

	endwtime = MPI_Wtime();
	cout << "wall clock time = " << endwtime-startwtime <<" secs" <<"\n";


	//запись функции распределения в файл
	cout<<"Записать F0?\n";
	cout<<"Если да, нажмите 1\n";
	cin>>save0;

	MPI_Send(&save0,1,MPI_DOUBLE,1,1,MPI_COMM_WORLD);
	MPI_Send(&save0,1,MPI_DOUBLE,2,2,MPI_COMM_WORLD);

	if(save0==1){
		
		FILE *fp;

		fp=fopen("/home/andrej/mnpt-023/andrejhmelev/F0_rezl.txt","w");

		for(p0=0;p0<rezl.it_v.size()*rezl.hx.size()*(rezl.hy.size()+1);p0++){
	
			fprintf(fp, "%0.15f \n",rezl.F0[p0]);
		
		}
		fclose(fp);
		
	}

	cout<<"Finish \n";
	
}

//обработка трубки
if(myid==1)
{
	int save1;
	int i1;
	int p1;
	int j1;
	int n1;
	int sloev1;
	double M11_begin;
	double M11_end;

	//создаём трубку
	int nx1,ny1,dny1;
	double lx1,ly1,dly1,TL1,TR1;

	int nksi1;
	double kc1;

	ifstream fin1 ("trubka.txt"); //открытие файла для ввода
	fin1>>lx1>>ly1>>dly1>>nx1>>ny1>>dny1>>TL1>>TR1;
	fin1.close();

	ifstream fin11 ("skorost.txt"); //открытие файла для ввода
	fin11>>nksi1>>kc1;
	fin11.close();

	Trubka trubka(lx1,ly1,dly1,nx1,ny1,dny1,TL1,TR1,nksi1,kc1);

	trubka.make_skorost();

	trubka.make_cells_y();//создаём ячейки и сетку по у
	trubka.make_cells_x();//создаём ячейки и сетку по х

	MPI_Barrier(MPI_COMM_WORLD);

//недостоющие элементы сетки
	
	//обмен данными с левым резервуаром
	MPI_Recv(&trubka.hx[0],1,MPI_DOUBLE,0,1,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&trubka.hx[1],1,MPI_DOUBLE,0,2,MPI_COMM_WORLD,&cStatus);

	MPI_Send(&trubka.hx[2],1,MPI_DOUBLE,0,3,MPI_COMM_WORLD);
	MPI_Send(&trubka.hx[3],1,MPI_DOUBLE,0,4,MPI_COMM_WORLD);

	trubka.X[1]=trubka.X[2]-trubka.hx[1];
	trubka.X[0]=trubka.X[1]-trubka.hx[1]/2-trubka.hx[0]/2;
	

	MPI_Send(&trubka.X[2],1,MPI_DOUBLE,0,3,MPI_COMM_WORLD);
	MPI_Send(&trubka.X[3],1,MPI_DOUBLE,0,4,MPI_COMM_WORLD);

	//обмен данными с правым резервуаром
	MPI_Recv(&trubka.hx[trubka.hx.size()-2],1,MPI_DOUBLE,2,3,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&trubka.hx[trubka.hx.size()-1],1,MPI_DOUBLE,2,4,MPI_COMM_WORLD,&cStatus);

	MPI_Send(&trubka.hx[trubka.hx.size()-4],1,MPI_DOUBLE,2,1,MPI_COMM_WORLD);
	MPI_Send(&trubka.hx[trubka.hx.size()-3],1,MPI_DOUBLE,2,2,MPI_COMM_WORLD);	

	MPI_Recv(&trubka.X[trubka.X.size()-2],1,MPI_DOUBLE,2,3,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&trubka.X[trubka.X.size()-1],1,MPI_DOUBLE,2,4,MPI_COMM_WORLD,&cStatus);

	trubka.X[trubka.hx.size()-2]=trubka.X[trubka.hx.size()-2]+trubka.Lx;
	trubka.X[trubka.hx.size()-1]=trubka.X[trubka.hx.size()-1]+trubka.Lx;



	trubka.make_kurant();//создаём числа куранта
	trubka.make_T();//создаём вектор температуры
	trubka.make_const();//создаём константы

	trubka.make_hi_func_teor();//создаём теоретическую хи функцию

	FILE *fp;

	fp=fopen("/home/andrej/mnpt-023/andrejhmelev/hi_func_teor.txt","w");
		for(i1=0;i1<trubka.hi_teor.size()-1;i1++){

			fprintf(fp, "%f %0.15f \n",trubka.x[i1],trubka.hi_teor[i1]);
		}
	fclose(fp);


	trubka.make_F_begin();//создаём начальное условие

	//загружаем функцию распределения из файла
	MPI_Recv(&save1,1,MPI_DOUBLE,0,1,MPI_COMM_WORLD,&cStatus);

	if(save1==1){

		ifstream fin1 ("F0_trubka.txt"); //открытие файла для ввода

		for(p1=0;p1<trubka.it_v.size()*trubka.hx.size()*(trubka.hy.size()+1);p1++){
		
		fin1>>trubka.F0[p1];
	
		}

		fin1.close();
	}	

	trubka.get_M();
	M11_begin=trubka.M;
	
	//временные слои
	sloev1=t/trubka.hy_min*trubka.ksicut;

	for(n1=1;n1<=sloev1;n1++){
		if(n1==1){
			trubka.make_Fyt(0.5);
		} else {
			trubka.make_Fyt(1.0);
		}
	
	//передаём данные
	for(j1=1;j1<=trubka.Ny;j1++){
		for(p1=0;p1<trubka.v_y.size();p1++){

	MPI_Send(&trubka.F0[3+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,0,1,MPI_COMM_WORLD);
	MPI_Send(&trubka.F0[2+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,0,2,MPI_COMM_WORLD);

	MPI_Recv(&trubka.F0[1+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,0,3,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&trubka.F0[0+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,0,4,MPI_COMM_WORLD,&cStatus);

		}
	}
	
	for(j1=1;j1<=trubka.Ny;j1++){
		for(p1=0;p1<trubka.v_y.size();p1++){

	MPI_Send(&trubka.F0[(trubka.hx.size()-4)+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,2,5,MPI_COMM_WORLD);
	MPI_Send(&trubka.F0[(trubka.hx.size()-3)+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,2,6,MPI_COMM_WORLD);

MPI_Recv(&trubka.F0[(trubka.hx.size()-2)+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,2,7,MPI_COMM_WORLD,&cStatus);
MPI_Recv(&trubka.F0[(trubka.hx.size()-1)+trubka.hx.size()*p1+trubka.v_y.size()*trubka.hx.size()*j1],1,MPI_DOUBLE,2,8,MPI_COMM_WORLD,&cStatus);

		}
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
		trubka.make_Fxt();

		if(n1==sloev1){
			
			trubka.make_Fyt(0.5);
		}
		
	}

	trubka.get_M();
	M11_end=trubka.M;

	MPI_Send(&M11_begin,1,MPI_DOUBLE,0,98,MPI_COMM_WORLD);
	MPI_Send(&M11_end,1,MPI_DOUBLE,0,96,MPI_COMM_WORLD);

	trubka.make_hi_func_real();//создаём хи функцию на основе расчётов

	FILE *fp1;

	fp1=fopen("/home/andrej/mnpt-023/andrejhmelev/hi_func_real.txt","w");

		for(i1=2;i1<trubka.hx.size()-2;i1++){
			
			fprintf(fp1, "%f %f \n",trubka.X[i1],trubka.hi_real[i1]);
			
		}

	fclose(fp1);

	MPI_Barrier(MPI_COMM_WORLD);

	//запись функции распределения в файл
	MPI_Recv(&save1,1,MPI_DOUBLE,0,1,MPI_COMM_WORLD,&cStatus);
	if(save1==1){
		
		FILE *fp1;

		fp1=fopen("/home/andrej/mnpt-023/andrejhmelev/F0_trubka.txt","w");

		for(p1=0;p1<trubka.it_v.size()*trubka.hx.size()*(trubka.hy.size()+1);p1++){
	
			fprintf(fp1, "%0.15f \n",trubka.F0[p1]);
		
		}

		fclose(fp1);
		
	}

}

//обработка правого резервуара
if(myid==2)
{
	int save2;
	int k2;
	int sloev2;
	int i2;
	int p2;
	int j2;
	int n2;
	double M22_begin;
	double M22_end;

	//создаём правый резервуар
	int nx2,ny2,dny2;
	double lx2,ly2,dly2,TL2,TR2;

	int nksi2;
	double kc2;

	ifstream fin2 ("rezr.txt"); //открытие файла для ввода
	fin2>>lx2>>ly2>>dly2>>nx2>>ny2>>dny2>>TL2>>TR2;
	fin2.close();

	ifstream fin22 ("skorost.txt"); //открытие файла для ввода
	fin22>>nksi2>>kc2;
	fin22.close();

	Rezr rezr(lx2,ly2,dly2,nx2,ny2,dny2,TL2,TR2,nksi2,kc2);

	rezr.make_skorost();

	rezr.make_cells_y();//создаём ячейки и сетку по у
	rezr.make_cells_x();//создаём ячейки и сетку по х

	MPI_Barrier(MPI_COMM_WORLD);

//недостоющие элементы сетки
	
	MPI_Send(&rezr.hx[2],1,MPI_DOUBLE,1,3,MPI_COMM_WORLD);
	MPI_Send(&rezr.hx[3],1,MPI_DOUBLE,1,4,MPI_COMM_WORLD);

	MPI_Recv(&rezr.hx[0],1,MPI_DOUBLE,1,1,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&rezr.hx[1],1,MPI_DOUBLE,1,2,MPI_COMM_WORLD,&cStatus);
	
	MPI_Send(&rezr.X[2],1,MPI_DOUBLE,1,3,MPI_COMM_WORLD);
	MPI_Send(&rezr.X[3],1,MPI_DOUBLE,1,4,MPI_COMM_WORLD);

	rezr.X[1]=rezr.X[2]-rezr.hx[1];
	rezr.X[0]=rezr.X[1]-rezr.hx[1]/2-rezr.hx[0]/2;



	rezr.make_kurant();//создаём числа куранта
	rezr.make_const();//создаём константы

	rezr.make_F_begin();//создаём начальное условие	

	//загружаем функцию распределения из файла
	MPI_Recv(&save2,1,MPI_DOUBLE,0,2,MPI_COMM_WORLD,&cStatus);

	if(save2==1){

		ifstream fin2 ("F0_rezr.txt"); //открытие файла для ввода

		for(p2=0;p2<rezr.it_v.size()*rezr.hx.size()*(rezr.hy.size()+1);p2++){
		
		fin2>>rezr.F0[p2];
	
		}

		fin2.close();
	}

	rezr.get_M();
	M22_begin=rezr.M;

	//временные слои
	sloev2=t/rezr.hy_min*rezr.ksicut;
	
	for(n2=1;n2<=sloev2;n2++){
		
		if(n2==1){
			rezr.make_Fyt(0.5);
		} else {
			rezr.make_Fyt(1.0);
		}
	
	//передаём данные
	for(j2=1;j2<=rezr.Ny;j2++){
		for(p2=0;p2<rezr.v_y.size();p2++){

	MPI_Recv(&rezr.F0[0+rezr.hx.size()*p2+rezr.v_y.size()*rezr.hx.size()*j2],1,MPI_DOUBLE,1,5,MPI_COMM_WORLD,&cStatus);
	MPI_Recv(&rezr.F0[1+rezr.hx.size()*p2+rezr.v_y.size()*rezr.hx.size()*j2],1,MPI_DOUBLE,1,6,MPI_COMM_WORLD,&cStatus);

	MPI_Send(&rezr.F0[2+rezr.hx.size()*p2+rezr.v_y.size()*rezr.hx.size()*j2],1,MPI_DOUBLE,1,7,MPI_COMM_WORLD);
	MPI_Send(&rezr.F0[3+rezr.hx.size()*p2+rezr.v_y.size()*rezr.hx.size()*j2],1,MPI_DOUBLE,1,8,MPI_COMM_WORLD);

		}
	}
	
	MPI_Barrier(MPI_COMM_WORLD);

		rezr.make_Fxt();
		
		if(n2==sloev2){
			
			rezr.make_Fyt(0.5);
		}
			
	}

	rezr.get_M();
	M22_end=rezr.M;

	MPI_Send(&M22_begin,1,MPI_DOUBLE,0,99,MPI_COMM_WORLD);
	MPI_Send(&M22_end,1,MPI_DOUBLE,0,97,MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);

	//запись функции распределения в файл
	MPI_Recv(&save2,1,MPI_DOUBLE,0,2,MPI_COMM_WORLD,&cStatus);

	if(save2==1){
		
		FILE *fp2;

		fp2=fopen("/home/andrej/mnpt-023/andrejhmelev/F0_rezr.txt","w");

		for(p2=0;p2<rezr.it_v.size()*rezr.hx.size()*(rezr.hy.size()+1);p2++){
	
			fprintf(fp2, "%0.15f \n",rezr.F0[p2]);
		
		}

		fclose(fp2);
		
	}

}
	

	MPI_Finalize();
	return 0;
}
