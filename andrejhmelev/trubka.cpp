#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <algorithm>
#include <vector>

#include "trubka.hpp"
#include "skorost.hpp"


using namespace std;

void Trubka::make_cells_y()
{
	//создаём сетку по у
		hy_min=Ly/Ny;
		a0=0.2;//alfa
		a1=0;
		
		for(p=0;p<=Ny+dNy;p++) {

			hy.push_back(0);
			Y.push_back(0);
			Y05.push_back(0);
			
		}

		for(p=1;p<=Ny;p++) {

			hy[p]=hy_min;

		}

		while((hy_min*(pow(1+a0,dNy)-1)/a0-dLy)>0.0000001) 
		{

			a1=a0-(a0*dLy-hy_min*(pow(1+a0,dNy)-1))/(dLy-hy_min*dNy*pow(1+a0,dNy-1));
			a0=a1;
		}

		for(p=1;p<=dNy;p++) {

			hy[Ny+p]=hy_min*pow(1+a0,p-1);

		}
		
		Y05[0]=0;
		Y[0]=Y05[0]-hy[1]/2;

		for(p=1;p<=Ny+dNy;p++){

			Y05[p]=Y05[p-1]+hy[p];
			Y[p]=Y05[p-1]+hy[p]/2;

		}
		
		Y.push_back(Y05[Ny+dNy]+hy_min*pow(1+a0,dNy)/2);
		
}

void Trubka::make_cells_x()
{

	//создаём сетку по х
		hx_min=Ly/Ny;
		a0=0.2;//alfa
		a1=0;
		
		for(p=0;p<=Nx+3;p++) {

			hx.push_back(0);
			X.push_back(0);
			X05.push_back(0);

		}

		Lx=Lx/2;
		Nx=Nx/2;

		while((hx_min*(pow(1+a0,Nx)-1)/a0-Lx)>0.0000001) 
		{
			a1=a0-(a0*Lx-hx_min*(pow(1+a0,Nx)-1))/(Lx-hx_min*Nx*pow(1+a0,Nx-1));
			a0=a1;
		}	

		for(p=2;p<=Nx+1;p++) {

			hx[p]=hx_min*pow(1+a0,p-2);

		}

		for(p=1;p<=Nx;p++) {

			hx[Nx+1+p]=hx_min*pow(1+a0,Nx-p);

		}
		
		Nx=2*Nx;
		Lx=2*Lx;

		for(p=2;p<=Nx+1;p++){

			X05[p]=X05[p-1]+hx[p];
			X[p]=X05[p-1]+hx[p]/2;

		}

}

void Trubka::make_kurant()
{
		for(p=0;p<v_x.size()*hx.size();p++){
			kur_x.push_back(0);
		}

		for(j=0;j<hx.size();j++){
			for(p=0;p<it_v.size();p++){

			kur_x[p+it_v.size()*j]=fabs(v_x[p])/ksicut*hx_min/hx[j];

			}
		}

		for(p=0;p<v_y.size()*hy.size();p++){
			kur_y.push_back(0);
		}

		for(j=1;j<hy.size();j++){
			for(p=0;p<v_y.size();p++){

			kur_y[p+v_y.size()*j]=fabs(v_y[p])/ksicut*hy_min/hy[j];

			}
		}
	
}

void Trubka::make_T()
{
	for(j=0;j<hx.size();j++){

	T_x.push_back(0);

	}

	for(j=2;j<hx.size()-2;j++){

		T_x[j]=(T_Rezl+(T_Rezr-T_Rezl)*X[j]/Lx);
		//T_x[j]=T_Rezr;

	}

}

void Trubka::make_const()
{	

	for(j=0;j<hx.size();j++){
		Sum_Epol_y.push_back(0);
	}

	for(p=0;p<hx.size()*v_y.size();p++){

		Epol_y.push_back(0);
		Eotr_y.push_back(0);

	}
	
	for(i=2;i<hx.size()-2;i++){
		for(p=0;p<v_y.size();p++){

			if(v_y[p]>0){
			Epol_y[i+hx.size()*p]=exp(((double) -1/2)*T_Rezl/T_x[i]*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
			Sum_Epol_y[i]=Sum_Epol_y[i]+fabs(v_y[p])*Epol_y[i+hx.size()*p];
			}

			if(v_y[p]<0){
			Eotr_y[i+hx.size()*p]=exp(((double) -1/2)*T_Rezl/T_x[i]*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
			}

		}
	}
	
}

void Trubka::make_F_begin()
{
	C=0;

	for(p=0;p<v_y.size()*hx.size()*(hy.size()+1);p++){

		F0.push_back(0);
		F05.push_back(0);
		th05.push_back(0);
		fi05.push_back(0);

	}

	//константа распределения
	for(p=0;p<v_y.size();p++){

		C=C+exp(((double) -1/2)*T_Rezl/T_Rezl*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
	}
	
	C=1/C*(N_ksi*N_ksi)/(ksicut*ksicut);

	//начальное условие
	for(i=2;i<hx.size()-2;i++){//по х
		for(p=0;p<v_y.size();p++){//по скоростям
			for(j=1;j<hy.size();j++){//по у

				//F0[i+hx.size()*p+v_y.size()*hx.size()*j]=C*exp(((double) -1/2)*T_Rezl/T_Rezl*(v_x[p]*v_x[p]+v_y[p]*v_y[p]+v_z[p]*v_z[p]));
				F0[i+hx.size()*p+v_y.size()*hx.size()*j]=0;

			}
		}
	}

}

void Trubka::get_M()
{
	M=0;
	M_pol=0;
	M_otr=0;
	for(i=2;i<hx.size()-2;i++){//по х
		for(p=0;p<v_y.size();p++){//по скоростям
			for(j=1;j<hy.size();j++){//по у
				M=M+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];
				if(v_x[p]>0){
				M_pol=M_pol+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];
				} else {
				M_otr=M_otr+F0[i+hx.size()*p+v_y.size()*hx.size()*j]*hy[j]*hx[i];	
				}
			}
		}
	}

}

int Trubka::make_Fyt(double l)
{
	vx=v_y.size()*hx.size();

	for(i=2;i<hx.size()-2;i++){//по всем столбцам по х
		for(p=0;p<v_y.size();p++){//по всем скоростям
			it_vx=i+hx.size()*p;
		
			if(v_y[p]>0) {

				F0[it_vx+vx*hy.size()]=std::max((double) 0,(double) 2*F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);

				for(j=2;j<=hy.size()-1;j++){
					if(((F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j])*(F0[it_vx+vx*j]-F0[it_vx+vx*(j-1)]))<=0){

						th05[it_vx+vx*j]=0;
						fi05[it_vx+vx*j]=0;

					} else {	
				
						th05[it_vx+vx*j]=(F0[it_vx+vx*j]-F0[it_vx+vx*(j-1)])/(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

						fi05[it_vx+vx*j]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*j])/kur_y[p+v_y.size()*j]*th05[it_vx+vx*j],
							(double) hy[j]/(((double) 2)*(Y[j+1]-Y[j-1]))*(1-kur_y[p+v_y.size()*j])*(1+th05[it_vx+vx*j]))));

					}

				F05[it_vx+vx*j]=F0[it_vx+vx*j]+fi05[it_vx+vx*j]*(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);	

				}

			} else {

				F0[it_vx+vx*0]=std::max((double) 0,(double) 2*F0[it_vx+vx*1]-F0[it_vx+vx*2]);

				for(j=0;j<=hy.size()-3;j++){
					if(((F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j])*(F0[it_vx+vx*(j+2)]-F0[it_vx+vx*(j+1)]))<=0){

						th05[it_vx+vx*j]=0;
						fi05[it_vx+vx*j]=0;

					} else {
	
						th05[it_vx+vx*j]=(F0[it_vx+vx*(j+2)]-F0[it_vx+vx*(j+1)])/(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

						fi05[it_vx+vx*j]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*(j+1)])/kur_y[p+v_y.size()*(j+1)]*th05[it_vx+vx*j],
						(double) hy[j+1]/(((double) 2)*(Y[j+2]-Y[j])))*(1-kur_y[p+v_y.size()*(j+1)]*(1+th05[it_vx+vx*j]))));
					}

				F05[it_vx+vx*j]=F0[it_vx+vx*(j+1)]-fi05[it_vx+vx*j]*(F0[it_vx+vx*(j+1)]-F0[it_vx+vx*j]);

				}
			}
	
		}



//дополнительные точки

		//F05[0] and F0[0] для положительных скоростей
				
		for (p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){
				for(k=0;k<v_y.size();k++){

					if((fabs(v_y[k]-(-1)*v_y[p])<0.000001) && (fabs(v_y[k]-(-1)*v_y[p])>=0) && (fabs(v_x[k]-v_x[p])<0.000001) && (fabs(v_x[k]-v_x[p])>=0) && (fabs(v_z[k]-v_z[p])<0.000001) && (fabs(v_z[k]-v_z[p])>=0))
					{

					F05[it_vx+vx*0]=F05[i+hx.size()*k+vx*0];
					F0[it_vx+vx*0]=F0[i+hx.size()*k+vx*1];

					}
				}
			}
		}



		//F05[N] otr

		Sum_Vpol_y=0;
	
		for (p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){

				Sum_Vpol_y=Sum_Vpol_y+fabs(v_y[p])*F05[it_vx+vx*(hy.size()-1)];

			}
		}
			
		for (p=0; p<it_v.size();p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]<0){

				F05[it_vx+vx*(hy.size()-1)]=Sum_Vpol_y/Sum_Epol_y[i]*Eotr_y[it_vx];

			}
		}



		//F05[1] pol
	
		for(p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){
				if(((F0[it_vx+vx*2]-F0[it_vx+vx*1])*(F0[it_vx+vx*1]-F0[it_vx+vx*0]))<=0){

					th05[it_vx+vx*1]=0;
					fi05[it_vx+vx*1]=0;

				} else {
					
					th05[it_vx+vx*1]=(F0[it_vx+vx*1]-F0[it_vx+vx*0])/(F0[it_vx+vx*2]-F0[it_vx+vx*1]);

					fi05[it_vx+vx*1]=std::max((double) 0,
							std::min((double) 1,
							std::min((double) (1-kur_y[p+v_y.size()*1])/kur_y[p+v_y.size()*1]*th05[it_vx+vx*1],
							(double) hy[1]/(((double) 2)*(Y[2]-Y[0]))*(1-kur_y[p+v_y.size()*1])*(1+th05[it_vx+vx*1]))));
				}
			F05[it_vx+vx*1]=F0[it_vx+vx*1]+fi05[it_vx+vx*1]*(F0[it_vx+vx*2]-F0[it_vx+vx*1]);

			}
		}	
	


		//F05[N-1] otr

		Sum_Vpol_y=0;

		for (p=0; p<v_y.size(); p++) {

		it_vx=i+hx.size()*p;

			if(v_y[p]>0){
			
			Sum_Vpol_y=Sum_Vpol_y+fabs(v_y[p])*(F0[it_vx+vx*(hy.size()-1)]+F0[it_vx+vx*hy.size()])/2;

			}
		}

		g_otr=0;

		for(p=0; p<v_y.size(); p++) {

			it_vx=i+hx.size()*p;

			if(v_y[p]<0){

			g_otr=Sum_Vpol_y/Sum_Epol_y[i]*Eotr_y[it_vx];

			F0[it_vx+vx*hy.size()]=std::max((double) 0,(double) 2*g_otr-F0[it_vx+vx*(hy.size()-1)]);

				if(((F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)])*(F0[it_vx+vx*hy.size()]-F0[it_vx+vx*(hy.size()-1)]))<=0){
					th05[it_vx+vx*(hy.size()-2)]=0;
					fi05[it_vx+vx*(hy.size()-2)]=0;

				} else {

					th05[it_vx+vx*(hy.size()-2)]=(F0[it_vx+vx*hy.size()]-F0[it_vx+vx*(hy.size()-1)])/
									(F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);


					fi05[it_vx+vx*(hy.size()-2)]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_y[p+v_y.size()*(hy.size()-1)])/kur_y[p+v_y.size()*(hy.size()-1)]*th05[it_vx+vx*(hy.size()-2)],
								(double) hy[hy.size()-1]/(((double) 2)*(Y[hy.size()]-Y[hy.size()-2]))*(1-kur_y[p+v_y.size()*(hy.size()-1)])*(1+th05[it_vx+vx*(hy.size()-2)]))));

				}

			F05[it_vx+vx*(hy.size()-2)]=F0[it_vx+vx*(hy.size()-1)]-fi05[it_vx+vx*(hy.size()-2)]*(F0[it_vx+vx*(hy.size()-1)]-F0[it_vx+vx*(hy.size()-2)]);

			g_otr=0;

			}
		}


//новый слой

		for(p=0;p<v_y.size();p++){

		it_vx=i+hx.size()*p;

			for(j=1;j<hy.size();j++){

				if(v_y[p]<0){

				F0[it_vx+vx*j]=F0[it_vx+vx*j]+l*kur_y[p+v_y.size()*j]*(F05[it_vx+vx*j]-F05[it_vx+vx*(j-1)]);

				} else {

				F0[it_vx+vx*j]=F0[it_vx+vx*j]-l*kur_y[p+v_y.size()*j]*(F05[it_vx+vx*j]-F05[it_vx+vx*(j-1)]);

				}
			}
		}



	}

return 0;
}








void Trubka::make_Fxt()
{
	for(j=1;j<hy.size();j++){//по всем столбцам по y
	vy=v_y.size()*hx.size()*j;
	

		for(p=0;p<v_x.size();p++){//по всем скоростям

			it_vy=hx.size()*p+vy;
		
			if(v_x[p]>0) {

				for(i=1;i<=hx.size()-3;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[i+it_vy]-F0[(i-1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {	
				
						th05[i+it_vy]=(F0[i+it_vy]-F0[(i-1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*i])/kur_x[p+v_x.size()*i]*th05[i+it_vy],
							(double) hx[i]/(((double) 2)*(X[i+1]-X[i-1]))*(1-kur_x[p+v_x.size()*i])*(1+th05[i+it_vy]))));

					}

				F05[i+it_vy]=F0[i+it_vy]+fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);	

				}

			} else {

				for(i=1;i<=hx.size()-3;i++){
					if(((F0[(i+1)+it_vy]-F0[i+it_vy])*(F0[(i+2)+it_vy]-F0[(i+1)+it_vy]))<=0){

						th05[i+it_vy]=0;
						fi05[i+it_vy]=0;

					} else {
	
						th05[i+it_vy]=(F0[(i+2)+it_vy]-F0[(i+1)+it_vy])/(F0[(i+1)+it_vy]-F0[i+it_vy]);

						fi05[i+it_vy]=std::max((double) 0,
								std::min((double) 1,
								std::min((double) (1-kur_x[p+v_x.size()*(i+1)])/kur_x[p+v_x.size()*(i+1)]*th05[i+it_vy],
						(double) hx[i+1]/(((double) 2)*(X[i+2]-X[i])))*(1-kur_x[p+v_x.size()*(i+1)]*(1+th05[i+it_vy]))));
					}

				F05[i+it_vy]=F0[(i+1)+it_vy]-fi05[i+it_vy]*(F0[(i+1)+it_vy]-F0[i+it_vy]);

				}
			}
	
		}


		//новый слой

		for(p=0;p<v_x.size();p++){

		it_vy=hx.size()*p+vy;

			for(i=2;i<hx.size()-2;i++){

				if(v_x[p]<0){

				F0[i+it_vy]=F0[i+it_vy]+kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				} else {

				F0[i+it_vy]=F0[i+it_vy]-kur_x[p+v_x.size()*i]*(F05[i+it_vy]-F05[(i-1)+it_vy]);

				}
				
			}
		}




}


}




void Trubka::make_hi_func_teor()
{
	for(i=0;i<=1001;i++){
		hi_teor.push_back(1);
		x.push_back(0);
		K.push_back(0);
	}

	for(i=0;i<=1001;i++){
		
		x[i]=Lx/(x.size()-2)*i;
	}

	a0=0;
	a1=0;
	
	
	
	while((hi_teor[1000]-a1)>0.0000001){
			a1=hi_teor[1000];
		for(i=0;i<=1000;i++){
			
			I=0;

			for(j=0;j<=1001;j++){
				I=I+hi_teor[j]/pow(1+(x[i]-x[j])*(x[i]-x[j]),((double) 3/2))*Lx/(x.size()-2);
			}
			

			K[i]=((double) 1/2)*(1-x[i]/pow(1+x[i]*x[i], 0.5))+((double) 1/2)*I;
			
		}
		for(i=0;i<=1001;i++){
		hi_teor[i]=K[i];
		}
	}

}

void Trubka::make_hi_func_real()
{
	P0=0;

	for(p=0;p<v_x.size();p++){

		it_vy=hx.size()*p+v_y.size()*hx.size()*3;

		if(v_x[p]>0){

			P0=P0+v_x[p]*F05[1+it_vy];

		}
	}


	for(i=0;i<hx.size()-1;i++){

		Py.push_back(0);
		hi_real.push_back(0);
	}


	for(i=2;i<hx.size()-2;i++){
		for(p=0;p<v_y.size();p++){

		it_vy=hx.size()*p+v_y.size()*hx.size()*(hy.size()-1);

			if(v_y[p]>0){

			Py[i]=Py[i]+v_y[p]*F05[i+it_vy];

			}
		}
	}

	for(i=2;i<hx.size()-2;i++){

		hi_real[i]=Py[i]/P0;
	}

}
