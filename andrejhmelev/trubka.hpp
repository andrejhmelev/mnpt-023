#pragma once
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <algorithm>
#include <vector>

#include "skorost.hpp"

using namespace std;

class Trubka:public Skorost //трубка
{
public:
	double Lx;//длина по х
	double Ly;//длина по y совпадающая с трубкой
	double dLy;//длина по у над трубкой
	int Nx;//ячеек по х
	int Ny;//ячеек по у сов.труб.
	int dNy;//ячеек по у над трубкой

	double hx_min;//минимальный шаг по сетке
	double x_min;//минимальная длина ячейки
	double hy_min;//минимальный шаг по сетке
	double y_min;//минимальная длина ячейки

	vector <double> X;//ячейки по х
	vector <double> X05;//середины ячеек по х
	vector <double> hx;//сетка по х
	vector <double> Y;//ячейки по у
	vector <double> Y05;//середины ячеек по у
	vector <double> hy;//сетка по у

	vector <double> kur_x;//числа куранта по х
	vector <double> kur_y;//числа куранта по у

	double T_Rezl; //температура в левом резервуаре
	double T_Rezr; //температура в правом резервуаре

	vector <double> T_x;//температура вдоль трубки

	vector <double> Sum_Epol_y;//знаменатель для положительных скоростей по у
	vector <double> Epol_y;//экспаненты для положительных скоростей по у
	vector <double> Eotr_y;//экспаненты для отрицательных скоростей по у
	double Sum_Vpol_y;//числитель для положительных скоростей по у
	double g_otr;//для поиска N точки для отрицательных скоростей

	//функции распределения
	vector <double> F0;
	vector <double> F05;
	double C;

	//масса
	double M_otr;
	double M_pol;
	double M;

	//лимитеры
	vector <double> fi05;
	vector <double> th05;
	

	Trubka(double lx, double ly, double dly, int nx, int ny, int dny,double TL, double TR, int nksi, double kc):Skorost(nksi,kc)
	{
		Lx=lx;
		Ly=ly;
		dLy=dly;
		Nx=nx;
		Ny=ny;
		dNy=dny;
		T_Rezl=TL;
		T_Rezr=TR;
	}

	void make_cells_x();//создаёт ячейки и сетку по х
	void make_cells_y();//создаёт ячейки и сетку по у
	void make_kurant();//создаёт вектора чисел куранта
	void make_T();//температура в трубке
	void make_const();//создаёт константы

	void make_F_begin();//создаём начальное условие и начальную массу

	void get_M();//получить массу трубки

	void make_Fxt();//пересчитать один временной слой по х
	int make_Fyt(double l);//пересчитать один временной слой по у

	void make_hi_func_teor();//теоретическая хи функция
	vector <double> hi_teor;
	void make_hi_func_real();//хи функция на основе расчётов
	vector <double> hi_real;

	vector <double> Py;//поток на стенку трубки
	double P0;//поток влетающий в трубку

	//переменные для расчёта теоретической хи функции
	double I;
	vector <double> x;
	vector <double> K;
 
private:
	double a0;
	double a1;
	int p,i,j,k;
	int it_vx,vx;
	int it_vy,vy;
	

};
